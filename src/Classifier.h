/*
 * Classifier.h
 *
 * Single Layer Classifier for linear classification
 *
 *  Created on: Jun 25, 2015
 *      Author: stephanie
 */

#ifndef CLASSIFIER_H_
#define CLASSIFIER_H_

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>


struct TuneParams
{
	double lowval;
	double highval;
	double scale;

	double bestval;
	double bestscale;

	bool logscale;
	int scalefactor;

	TuneParams(double lv=1e-7, double hv=1e-1, double s=10, double bv=0.0, double bs=10, double ls=true, double sf=10) :
	            	  lowval(lv), highval(hv), scale(s), bestval(bv), bestscale(bs), logscale(ls), scalefactor(sf) {}

	void updateParams(){
		lowval = bestval-scale;
		highval = bestval + scale;
		scale /= scalefactor;
	}

	void updateParamsLHS(double coarse_scale){
		lowval = coarse_scale/scalefactor;
		highval = coarse_scale;
		scale = coarse_scale/scalefactor;
	}

	void updateParamsRHS(double coarse_scale){
		lowval = coarse_scale;
		highval = coarse_scale*scalefactor;
		scale = coarse_scale;
	}
};


class Classifier {
public:
	Classifier();
	virtual ~Classifier();
	Classifier(int nc, int nd);
	virtual double calculate_loss(cv::Mat &X, cv::Mat &y, double reg=0.0, bool UPDATE_GRAD=true) = 0;
	void grad_check_sparse(cv::Mat &X, cv::Mat &y, int num_checks=10, double reg=0.0, double h=0.00001);
	double* train(cv::Mat &X, cv::Mat &y, double learning_rate=0.001, double reg=0.00001,
			int num_iters=1500, int batch_size=200, bool verbose=true);
	double* train_batched(cv::Mat &X, cv::Mat &y, double learning_rate=0.001, double reg=0.00001,
			int num_iters=1500, int batch_size=200, bool verbose=true);

	void predict(cv::Mat &X, cv::Mat &y_pred);
	void tune_hyperparameters(cv::Mat &trainX, cv::Mat &trainy, cv::Mat &valX, cv::Mat &valy, int precision_level=0);

protected:

	int num_classes;
	int num_dims;
	cv::Mat W; // weights
	cv::Mat dW;

	cv::Mat bestW;
	cv::Mat bestdW;

	static const int CVTYPE_IN = CV_64FC1;
	static const int CVTYPE_OUT = CV_8UC1;

	void initializeWeights();
	double singleLoop(cv::Mat &trainX, cv::Mat &trainy, cv::Mat &valX, cv::Mat &valy,
			int num_iters, int batch_size, double learning_rate, double reg,bool verbose=false);
	double singleround(cv::Mat &trainX, cv::Mat &trainy, cv::Mat &valX, cv::Mat &valy,
					int num_iters, int batch_size, TuneParams* lr, TuneParams* rn, double* best_val, bool verbose);
};


// Derived class
class LinearSVM: public Classifier
{
   public:
	  LinearSVM(int num_classes, int num_dims) : Classifier(num_classes, num_dims){};

      double calculate_loss(cv::Mat &X, cv::Mat &y, double reg=0.0, bool UPDATE_GRAD=true);
};

// Derived class
class SoftMax: public Classifier
{
   public:
	  SoftMax(int num_classes, int num_dims) : Classifier(num_classes, num_dims){};

      double calculate_loss(cv::Mat &X, cv::Mat &y, double reg=0.0, bool UPDATE_GRAD=true);
};

#endif /* CLASSIFIER_H_ */
