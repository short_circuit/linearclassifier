/*
 * Cifar.cpp
 *
 *  Created on: Jun 25, 2015
 *      Author: stephanie
 */

#include "Cifar.h"

//String constant declarations
const string Cifar::ROOT_DIR = "..";
const string Cifar::DATA_DIR = "/datasets/cifar-10-batches-bin/";
const string Cifar::TEST_FILE = "test_batch.bin";
const string Cifar::TRAIN_FILE = "data_batch_";

	/** Default constructor */
	Cifar::Cifar()
	{
		training_num = 49000;
		validation_num = 1000;
		test_num = 10000;
		num_dims = 0;
		do_subtract_mean = true;
		do_add_bias = false;
	}

  /** Copy constructor */
	Cifar::Cifar (const Cifar& other){
		// TODO
  }

  /** Destructor */
  Cifar::~Cifar(){
		// TODO
	  all_images.release();
	  test_images.release();
	  mean_image.release();
  }

  /** Assignment operator */
  Cifar& Cifar::operator= (const Cifar& other)
  {
	  // TODO
	   return *this;
  }

	/** Other constructor  */
	Cifar::Cifar(int trn, int van, int ten, bool sub_mean, bool ab){
		training_num = trn;
		validation_num = van;
		test_num = ten;
		num_dims = 0;
		do_subtract_mean = sub_mean;
		do_add_bias = ab;
	}

	void Cifar::setBias(bool sb){

		do_add_bias = sb;
	}

int Cifar::loadCIFAR(){

	if(load_CIFAR_batch()){
		return 1;
	}

	// Shallow copy to train and validation sets
   train_images = all_images.rowRange(0,training_num);
   validation_images = all_images.rowRange(training_num,all_images.rows);

   train_labels = all_labels.rowRange(0,training_num);
   validation_labels = all_labels.rowRange(training_num,all_images.rows);

	// subtract the mean image from training, validation and test data
   if (do_subtract_mean){
	std::cout << "Subtracting mean image from data ...." << std::endl;
	subtract_mean(train_images,mean_image,true);
	subtract_mean(validation_images,mean_image,false);
	subtract_mean(test_images,mean_image,false);
   }

	// add a bias dimension
   if (do_add_bias){
	std::cout << "Adding bias dimension ...." << std::endl;
	add_bias(train_images);
	add_bias(validation_images);
	add_bias(test_images);
   }

	num_dims = train_images.cols;

	std::cout << "Data loaded" << std::endl;
	return 0;
}

int Cifar::load_CIFAR_batch(){
	 for (int b = 1; b < FILE_NUMS+1;b++){
		 std::stringstream filename;
		 filename << ROOT_DIR << DATA_DIR << TRAIN_FILE << b << ".bin";
		 string ss = filename.str();

		 cv::Mat labels;
		 cv::Mat images;
		 if(!load_CIFAR_file(ss, images, labels)){
			std::cout << "Successfully loaded file" << std::endl;
			// Join batches
			if (all_images.cols == 0 && all_images.rows == 0){
				all_images = images;
				all_labels = labels;
			} else {
				vconcat(all_images,images,all_images);
				vconcat(all_labels,labels,all_labels);
			}
		 } else {
			 return 1;
		 }
		 filename.clear();
		 labels.release();
		 images.release();
	 }

	 all_images.convertTo(all_images, CVTYPE_IN);
	 all_labels.convertTo(all_labels,CVTYPE_OUT);

	 // do test batch
	 std::stringstream filename;
	 filename << ROOT_DIR << DATA_DIR << TEST_FILE;
	 string ss = filename.str();
	 cv::Mat labels;
	 cv::Mat images;
	 if(!load_CIFAR_file(ss, images, labels)){
		// stuff to do
		std::cout << "Successfully loaded file" << std::endl;
	 }
		test_images = images;
		test_labels = labels;
	 filename.clear();
	 labels.release();
	 images.release();

	 test_images.convertTo(test_images, CVTYPE_IN);
	 test_labels.convertTo(test_labels, CVTYPE_OUT);

	 return 0;
}

int  Cifar::load_CIFAR_file(string filename, cv::Mat &images, cv::Mat &labels){

	int total_size = IM_NUM*(IM_SIZE+1);

	std::ifstream file(filename.c_str(), std::ios::in|std::ios::binary|std::ios::ate);
	if (file.is_open())
	{
	  int size = file.tellg();
	  if (size==total_size){
		  file.seekg(0, std::ios::beg);
         images = cv::Mat::zeros(IM_NUM, IM_SIZE, CV_8UC1);
         labels = cv::Mat::zeros(IM_NUM, 1, CVTYPE_OUT);
		  for (unsigned int in = 0; in < IM_NUM; in++){
			  char l_tmp;
			  file.read(&l_tmp, 1); // 1 byte
			  labels.at<char>(in,0) = l_tmp;
			  char* imdata = new char[IM_SIZE];
			  file.read(imdata, IM_SIZE); // 1 image
			  for (unsigned int j =0; j < IM_SIZE; j++){
				  images.at<uchar>((int)in,(int)j) = (char)imdata[j];
			  }
		}
		  file.close();
		  return 0;
	  } else {
		  file.close();
		  return 2;
	  }
	  file.close();
	}
	else
	{
		 std::cout << "Unable to open file " << filename << std::endl;
		 return 1;
	}
}

int Cifar::add_bias(cv::Mat &image_set)
{
	int im_num = image_set.rows;
	cv::Mat bias_vec = cv::Mat::ones(im_num, 1, CVTYPE_IN);
	hconcat(bias_vec,image_set,image_set);
}

int Cifar::subtract_mean(cv::Mat &image_set,cv::Mat &row_mean, bool CALC_MEAN){

	// other error handling

	if (CALC_MEAN){
			cv::reduce(image_set,row_mean, 0, CV_REDUCE_AVG);
	}

	for (int r = 0; r < image_set.rows; r++) {
		image_set.row(r) = image_set.row(r) - row_mean;
	}
}

cv::Mat& Cifar::getTrainI(){
	return train_images;
}

cv::Mat& Cifar::getTrainL(){
	return train_labels;
}

cv::Mat& Cifar::getValI(){
	return validation_images;
}

cv::Mat& Cifar::getValL(){
	return validation_labels;
}

cv::Mat& Cifar::getTestI(){
	return test_images;
}

cv::Mat& Cifar::getTestL(){
	return test_labels;
}

int Cifar::getNumClasses(){
	return NUM_CLASSES;
}

int Cifar::getNumDims(){
	return num_dims;
}

