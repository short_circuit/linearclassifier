/*
 * Cifar.h
 *
 *  Created on: Jun 25, 2015
 *      Author: stephanie
 */

#ifndef CIFAR_H_
#define CIFAR_H_

#include <iostream>
#include <string>
#include <fstream>
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using std::string;

class Cifar
{
public:

	/** Default constructor */
	Cifar();

	/** Copy constructor */
	Cifar (const Cifar& other);

	/** Destructor */
	~Cifar();

	/** Assignment operator */
	Cifar& operator= (const Cifar& other);

	/** Other constructor  */
	Cifar(int trn, int van, int ten, bool sub_mean=true, bool ab=false);

	int loadCIFAR();
	void setBias(bool sb=true);

	cv::Mat& getTrainI();
	cv::Mat& getTrainL();
	cv::Mat& getValI();
	cv::Mat& getValL();
	cv::Mat& getTestI();
	cv::Mat& getTestL();
	int getNumClasses();
	int getNumDims();

	private:

		int training_num;
		int validation_num;
		int test_num;
		int num_dims;

		bool do_subtract_mean;
		bool do_add_bias;

		cv::Mat all_images;
		cv::Mat all_labels;

		cv::Mat train_images;
		cv::Mat train_labels;
		cv::Mat validation_images;
		cv::Mat validation_labels;

		cv::Mat test_images;
		cv::Mat test_labels;

		cv::Mat mean_image;

		static const int IM_NUM = 10000;
		static const int ROWS = 32;
		static const int COLS = 32;
		static const int CHANNELS = 3;
		static const int IM_SIZE = ROWS*COLS*CHANNELS;
		static const int FILE_NUMS = 5;
		static const int NUM_CLASSES = 10;
		static const int CVTYPE_IN = CV_64FC1;
		static const int CVTYPE_OUT = CV_8UC1;
		static const string ROOT_DIR;
		static const string DATA_DIR;
		static const string TEST_FILE;
		static const string TRAIN_FILE;

		int load_CIFAR_batch();
		int load_CIFAR_file(string filename, cv::Mat &images, cv::Mat &labels);

		int subtract_mean(cv::Mat &image_set,cv::Mat &row_mean, bool CALC_MEAN);
		int add_bias(cv::Mat &image_set);
};

#endif /* CIFAR_H_ */
