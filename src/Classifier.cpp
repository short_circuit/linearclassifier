/*
 * Classifier.cpp
 *
 *  Created on: Jun 25, 2015
 *      Author: stephanie
 */

#include "Classifier.h"

Classifier::Classifier() {
	// TODO Auto-generated constructor stub
	num_classes = 0;
	num_dims = 0;

}

Classifier::~Classifier() {
	// TODO Auto-generated destructor stub
}

Classifier::Classifier(int nc, int nd){
	num_classes = nc;
	num_dims = nd;

	initializeWeights();

}

double* Classifier::train(cv::Mat &X, cv::Mat &y, double learning_rate, double reg, int num_iters,
          int batch_size, bool verbose){
/*""
   Train the linear classifier using stochastic gradient descent.
   """ */
    int num_samples = X.rows;

	srand (time(NULL));

   double* loss_history = new double[num_iters];
   double loss;
   cv::Mat X_batch = cv::Mat::zeros(batch_size,num_dims,CVTYPE_IN);
   cv::Mat y_batch = cv::Mat::zeros(batch_size,1,CVTYPE_OUT);
   int ix;

   for(int i = 0; i < num_iters; i++){

	 // select batch_size random numbers
	 // select batch_size random elements from training data
	  for(int j = 0; j < batch_size; j++){
		    ix = rand() % num_samples;
		    X.row(ix).copyTo(X_batch.row(j));
		   	y.row(ix).copyTo(y_batch.row(j));
	  }
     // evaluate loss and gradient
	  loss = calculate_loss(X_batch, y_batch, reg);
	  loss_history[i] = loss;

    // perform weight update
	 W -= learning_rate * dW; // new position in the weight space

     if (verbose && !(i % 100)){
       std::cout << "iteration " << i << " / " << num_iters << ": loss " << loss << std::endl;
     }
	}
   return loss_history;
}

void Classifier::grad_check_sparse(cv::Mat &X, cv::Mat &y, int num_checks, double reg, double h){
 /* """
  sample a few random elements and only return numerical
  in this dimensions.
  """ */

  /* initialize random seed: */
  srand (time(NULL));

  // set UPDATE_GRAD = false;
  bool UPDATE_GRAD = false;

  for (int i =0; i < num_checks; i++){
    int ix = rand() % num_classes;
    int iy = rand() % num_dims;

    // evaluate f(x + h)
    W.at<double>(ix,iy) += h; // increment by h

    cv::Mat temp;
    double fxph = calculate_loss(X, y, reg,UPDATE_GRAD);

    W.at<double>(ix,iy) -= 2 * h; // decrement by h
    temp.release();

    // evaluate f(x - h)
    double fxmh = calculate_loss(X, y, reg,UPDATE_GRAD);

    W.at<double>(ix,iy) += h; // reset

    double grad_numerical = (fxph - fxmh) / (2 * h);
    double grad_analytic = dW.at<double>(ix,iy);
    double rel_error = std::abs(grad_numerical - grad_analytic) / (std::abs(grad_numerical) + std::abs(grad_analytic));
    std::cout << "numerical: " << grad_numerical << " analytic: " << grad_analytic << " relative error: " << rel_error << std::endl;
  }
}

void Classifier::predict(cv::Mat &X, cv::Mat &y_pred){

	int num_samples = X.rows;
	y_pred = cv::Mat::zeros(num_samples,1,CVTYPE_OUT);

	cv::Mat scores = W * X.t();

	// have to loop
	for(int i = 0; i < num_samples; i++){
		cv::Point p1;
		cv::minMaxLoc(scores.col(i),0,0,0,&p1);
		y_pred.at<uchar>(i) = p1.y;
	}
}

// calculate loss
double LinearSVM::calculate_loss(cv::Mat &X, cv::Mat &y, double reg, bool UPDATE_GRAD){
		 /* """
		  SVM loss function
		  """*/

		if (UPDATE_GRAD){
		  dW = cv::Mat::zeros(W.size(),CVTYPE_IN); // initialize the gradient as zero
		}
		  // compute the loss and the gradient
		  int num_train = X.rows;

		  cv::Mat Xi = cv::Mat::zeros(num_dims,1,CVTYPE_IN);

		  cv::Mat scores = W * X.t();

		  double loss = 0.0;
		  for (int i = 0; i < num_train; i++){
			Xi = X.row(i);
			int correct_class = y.at<char>(i,0);
			double correct_class_score = scores.at<double>(correct_class,i);
			cv::Mat marginM = scores.col(i) - correct_class_score + 1; // note delta = 1
		    for (int j = 0; j < num_classes; j++){
		    	if (j==correct_class){
		    		continue;
		    	}
		    	double margin = marginM.at<double>(j,0);
		    	if (margin > 0){
		    		loss += margin;
		    		// calc dW
		    		if (UPDATE_GRAD){
						dW.row(j) += Xi;
						dW.row(correct_class) -= Xi;
		    		}
		    	}
		    }
		  }

		  // Right now the loss is a sum over all training examples, but we want it
		  // to be an average instead so we divide by num_train.
		  loss /= num_train;
    	if (UPDATE_GRAD){
    			dW /= num_train;
    		}
		  // Add regularization to the loss.
		  loss += 0.5 * reg * cv::sum(W.mul(W))[0];

		  return loss;
		  }

// calculate loss - SoftMax version
double SoftMax::calculate_loss(cv::Mat &X, cv::Mat &y, double reg, bool UPDATE_GRAD){
		 /* """
		  Softmax loss function
		  """"*/
		if (UPDATE_GRAD){
		  dW = cv::Mat::zeros(W.size(),CVTYPE_IN); // initialize the gradient as zero
		}
		  // compute the loss and the gradient
		  int num_train = X.rows;

		  cv::Mat Xi = cv::Mat::zeros(num_dims,1,CVTYPE_IN);
		  cv::Mat scores = W * X.t();

		  double loss = 0.0;

		  cv::Mat maxScores;
		  cv::reduce(scores, maxScores, 0, CV_REDUCE_MAX);
		  for (int i = 0; i < num_train; i++){
			  Xi = X.row(i);
			  int correct_class = y.at<char>(i,0);
			  cv::Mat sampleScores = scores.col(i);
			  sampleScores = sampleScores - maxScores.at<double>(i);
			  cv::Mat expScores;
			  cv::exp(sampleScores,expScores);
			  double denom = cv::sum(expScores)[0];
			  cv::Mat p = expScores / denom;
			  double correctScore = p.at<double>(correct_class,0);
			  loss += -log(correctScore);

			  if (UPDATE_GRAD){
				  p.at<double>(correct_class,0) -= 1.0;
				  for (int j = 0; j < num_classes; j++){
				  	  dW.row(j) += p.at<double>(j,0)*Xi;
				  }
			  }
		  }

		  // Right now the loss is a sum over all training examples, but we want it
		  // to be an average instead so we divide by num_train.
		  loss /= num_train;
    	if (UPDATE_GRAD){
    			dW /= num_train;
    		}
		  // Add regularization to the loss.
		  loss += 0.5 * reg * cv::sum(W.mul(W))[0];
		  return loss;
	  }


void Classifier::initializeWeights(){
	// Randomly initialize weights
	srand (time(NULL));

	W = cv::Mat::zeros(num_classes,num_dims,CVTYPE_IN);
	cv::randn(W, 0.0, 1.0);
	W *= 0.0001;

	dW = cv::Mat::zeros(num_classes,num_dims, CVTYPE_IN);
	cv::randn(dW, 0.0, 1.0);
	dW *= 0.0001;
}

void Classifier::tune_hyperparameters(cv::Mat &trainX, cv::Mat &trainy, cv::Mat &valX, cv::Mat &valy, int precision_level){

//# Use the validation set to tune hyperparameters (regularization strength and
//# learning rate).

	double best_val = 0.0;

	// train SVM
	std::cout << "Tuning learning rate and regularization strength ...." << std::endl;
	std::cout << " " << std::endl;

	// hard-coded values
	int num_iters = 10;
	int batch_size = 20;
	double lowestres = 1e-9;
	int start_scale = 10;

	TuneParams* lr = new TuneParams(lowestres,10,start_scale,best_val,start_scale,true,start_scale);
	TuneParams* rn = new TuneParams(lowestres,10,start_scale,best_val,start_scale,true,start_scale);

	// First round is a log round to determine the scale
	std::cout << "Coarse determination of scale ...." << std::endl;
	std::cout << " " << std::endl;

	best_val = singleround(trainX, trainy,valX, valy, num_iters, batch_size, lr, rn, &best_val, true);

	double coarse_lr = lr->bestval;
	double coarse_rn = rn->bestval;
	lr->logscale = false;
	rn->logscale = false;
	lr->bestscale = coarse_lr;
	rn->bestscale = coarse_rn;

	time_t now;
	time_t after;

	// Determine the correct scale quadrant.
	std::cout << "Determining precise scale...." << std::endl;
	if (precision_level > 0){
		lr->updateParamsLHS(coarse_lr);
		rn->updateParamsLHS(coarse_rn);
		singleround(trainX, trainy,valX, valy, num_iters, batch_size, lr, rn, &best_val, false);

		lr->updateParamsRHS(coarse_lr);
		singleround(trainX, trainy,valX, valy, num_iters, batch_size, lr, rn, &best_val, false);

		rn->updateParamsRHS(coarse_rn);
		singleround(trainX, trainy,valX, valy, num_iters, batch_size, lr, rn, &best_val, false);

		lr->updateParamsLHS(coarse_lr);
		singleround(trainX, trainy,valX, valy, num_iters, batch_size, lr, rn, &best_val, false);

	} else {
		// only vary one parameter at a time
		TuneParams* rn2 = new TuneParams(rn->bestval,rn->bestval+1e-7,1,rn->bestval,1,false,1);

		lr->updateParamsLHS(coarse_lr);
		singleround(trainX, trainy,valX, valy, num_iters, batch_size, lr, rn2, &best_val, false);

		lr->updateParamsRHS(coarse_lr);
		singleround(trainX, trainy,valX, valy, num_iters, batch_size, lr, rn2, &best_val, false);

		TuneParams* lr2 = new TuneParams(lr->bestval,lr->bestval+1e-7,1,lr->bestval,1,false,1);

		rn->updateParamsRHS(coarse_rn);
		singleround(trainX, trainy,valX, valy, num_iters, batch_size, lr2, rn, &best_val, false);

		rn->updateParamsLHS(coarse_rn);
		singleround(trainX, trainy,valX, valy, num_iters, batch_size, lr2, rn, &best_val, false);
	}


	if (precision_level > 2){
	 lr->scale = lr->bestscale;
	 rn->scale = rn->bestscale;
	 double initial_scale = rn->scale;

	// loop through from start_scale to lowest_res
	std::cout << "Improving precision...." << std::endl;
		num_iters*=20; // Increase the number of iterations
		batch_size*=10;
		while (lr->scale > lowestres){
			lr->updateParams();
			rn->scale = initial_scale;
			while (rn->scale > lowestres){
				rn->updateParams();
				now = time(0);
				best_val = singleround(trainX, trainy,valX, valy, num_iters, batch_size, lr, rn, &best_val, false);
				after = time(0);
				std::cout << "That took " << after - now << " s" << std::endl;
			}
		}
	}

	// do a final round of training
	std::cout << "Final tuning of classifier..."  << std::endl;
	num_iters = 1500; // Increase the number of iterations
	batch_size = 200;
	train(trainX, trainy, lr->bestval, rn->bestval, num_iters, batch_size, false);

	std::cout << "Best validation accuracy achieved during cross-validation: " << best_val << std::endl;
}

double Classifier::singleround(cv::Mat &trainX, cv::Mat &trainy, cv::Mat &valX, cv::Mat &valy,
				int num_iters, int batch_size, TuneParams* lr, TuneParams* rn, double* best_val, bool verbose){
	double validation_accuracy = 0.0;

	// train classifier using current hyperparameter settings
		double learning_rate = lr->lowval;
		if (verbose){
			std::cout << "Learning rate range: " << lr->lowval << " to " << lr->highval << std::endl;
			std::cout << "Regularization range: " << rn->lowval << " to " << rn->highval << std::endl;
		}

		while (learning_rate < lr->highval){

			double reg = rn->lowval;

			while (reg < rn->highval){
				validation_accuracy = singleLoop(trainX, trainy, valX, valy,
						num_iters, batch_size, learning_rate, reg, verbose);

				if (validation_accuracy > (*best_val)){
					lr->bestval = learning_rate;
					rn->bestval = reg;
					rn->bestscale = rn->scale;
					lr->bestscale = lr->scale;
					(*best_val)  = validation_accuracy;
				}

				if (rn->logscale){
					reg *= rn->scale;
				} else {
					reg += rn->scale;
				}
			}

			if (lr->logscale){
				learning_rate *= lr->scale;
			} else {
				learning_rate += lr->scale;
			}
		}

		std::cout << (lr->logscale ? "Log increment: " : "Increment: ") << lr->scale << " Best learning rate so far: " << lr->bestval<< std::endl;
		std::cout << (rn->logscale ? "Log increment: " : "Increment: ")  << rn->scale << " Best regularization value so far: " << rn->bestval << std::endl;
		std::cout << "Validation accuracy: " << (*best_val) << std::endl;
		std::cout << std::endl;

		return (*best_val);
}

double Classifier::singleLoop(cv::Mat &trainX, cv::Mat &trainy, cv::Mat &valX, cv::Mat &valy,
		int num_iters, int batch_size, double learning_rate, double reg, bool verbose){

	double training_accuracy;
	double validation_accuracy;

	cv::Mat y_pred;
	cv::Mat diff;

	if (verbose){
			std::cout << "Training with learning rate " << learning_rate << " and reg " << reg << std::endl;
		}
		// clear weights for a fair hyperparameter test
		initializeWeights();

		train(trainX, trainy, learning_rate, reg, num_iters, batch_size, false);

		// predict on training data and compare to ground truth
		predict(trainX, y_pred);
		diff = (y_pred == trainy);
		training_accuracy = (double)cv::countNonZero(diff) / y_pred.rows;

		// predict on validation data and compare to ground truth
		predict(valX, y_pred);
		diff = (y_pred == valy);
		validation_accuracy = (double)cv::countNonZero(diff) / y_pred.rows;
		if (verbose){
			std::cout << "Training accuracy " << training_accuracy << ", Validation accuracy " << validation_accuracy << std::endl;
		}

		return validation_accuracy;
}
