/*
 * main.cpp
 *
 *  Created on: Jun 25, 2015
 *      Author: stephanie
 */

#include <iostream>

#include "src/Classifier.h"
#include "src/Cifar.h"

bool selectClassifierType();
bool selectActionType();
void showPredictionResults(Classifier* lclass, Cifar* cifar);
void doPrediction(Classifier* lclass, Cifar* cifar,	int i=2);

int main(){

	bool TESTING = false;
	bool dosvm = true;
	Classifier* lclass;

	Cifar* cifar = new Cifar();
	// Because we are doing single-layer linear classification, add a bias dimension
	cifar->setBias(true);

	if(cifar->loadCIFAR()){
		delete cifar;
		return -1;
	}

	if (TESTING){
		if (dosvm){
			lclass = new LinearSVM(cifar->getNumClasses(),cifar->getNumDims());
		} else {
			lclass = new SoftMax(cifar->getNumClasses(),cifar->getNumDims());
		}
		lclass->calculate_loss(cifar->getTrainI(), cifar->getTrainL(), 0.0);
		lclass->grad_check_sparse(cifar->getTrainI(), cifar->getTrainL());

		return 0;
	}

	dosvm = selectClassifierType();

	if (dosvm){
		lclass = new LinearSVM(cifar->getNumClasses(),cifar->getNumDims());
	} else {
		lclass = new SoftMax(cifar->getNumClasses(),cifar->getNumDims());
	}

	bool single_run = selectActionType();

	if (single_run){
		// Train with default parameters
		time_t now = time(0);
		lclass->train(cifar->getTrainI(), cifar->getTrainL(), 1e-7, 5e-4, 1500, 200, false);
		time_t after = time(0);
		std::cout << "That took " << after - now << " s" << std::endl;
		showPredictionResults(lclass, cifar);
	} else {
		int precision_level;
		std::cout << "Select level of tuning from 0 (low), 1 (medium) or 2 (high but exceedingly slow - not recommended): ";
		std::cin >> precision_level;
		lclass->tune_hyperparameters(cifar->getTrainI(), cifar->getTrainL(),cifar->getValI(),cifar->getValL(),precision_level);
		showPredictionResults(lclass,cifar);
	}

	delete cifar;
	return 0;
}

bool selectClassifierType(){

	bool dosvm = true;
	int loss_type;

	std::cout << "Select Classifier Type (1:SVM, 2:SoftMax): ";
	std::cin >> loss_type;

	if (loss_type == 1){
		dosvm = true;
	} else if (loss_type==2){
		dosvm = false;
	} else {
		dosvm = true;  // SVM default
	}

	std::cout << "You selected " << loss_type << ". Initializing " << (dosvm ? "SVM" : "SoftMax") << " classifier..." << std::endl;

	return dosvm;
}

bool selectActionType(){

	bool singlerun = true;

	int run_type;

	std::cout << "Select Action (1:Single Run, 2:Full System Tune (takes a while)): ";
	std::cin >> run_type;

	if (run_type == 2){
		singlerun = false;
	}
	std::cout << "You selected " << run_type << ". Starting " << (singlerun ? "Single Run" : "System Tune") << "..." << std::endl;

	return singlerun;
}


void showPredictionResults(Classifier* lclass, Cifar* cifar){
	doPrediction(lclass,cifar, 0);
	doPrediction(lclass,cifar, 1);
	doPrediction(lclass,cifar, 2);
}

void doPrediction(Classifier* lclass, Cifar* cifar, int i){

	cv::Mat X;
	cv::Mat y;
	cv::Mat y_pred;
	std::string set_type;

	if (i==0){
		X = cifar->getTrainI();
		y = cifar->getTrainL();
		set_type = "training";
	} else if (i==1){
		X = cifar->getValI();
		y = cifar->getValL();
		set_type = "validation";
	} else {
		X = cifar->getTestI();
		y = cifar->getTestL();
		set_type = "test";
	}

	// predict on training data and compare to ground truth
	std::cout << "Calculating system accuracy on " << set_type << " data:" << std::endl;
	lclass->predict(X, y_pred);
	cv::Mat diff = (y_pred == y);
	std::cout << set_type << " accuracy: " <<  ((double)cv::countNonZero(diff) / y_pred.rows) << std::endl;
}
